const getSum = (str1, str2) => {
  if (typeof str1 !== "string" || typeof  str2 !== "string"){
    return false;
  }
  if (Number.isNaN(+str1) || Number.isNaN(+str2)){
    return false;
  }
  return String(BigInt(str1) + BigInt(str2));
};

const getQuantityPostsByAuthor = (listOfPosts, authorName) => {
  let postCount = 0,
      commentsCount = 0;
  listOfPosts.forEach((post) => {
    postCount += post.author === authorName || 0;
    if (post.comments){
      post.comments.forEach((comment) => {
        commentsCount += comment.author === authorName || 0;
      })
    }
  });
  return `Post:${postCount},comments:${commentsCount}`;
};

const tickets=(people)=> {
  const alreadyBoth = [];
  let canSell = true;
  people.forEach((price) => {
    if (price==25){
      alreadyBoth.push(+price);
      return;
    }
    let sum = alreadyBoth.reduce((pre,current) => pre+current,0);
    if (sum<+price-25){
      canSell = false;
    }
  })
  return canSell ? 'YES' : 'NO';
};


module.exports = {getSum, getQuantityPostsByAuthor, tickets};
